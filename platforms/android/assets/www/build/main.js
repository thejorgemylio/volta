webpackJsonp([5],{

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BodegaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__residuos_residuos__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_bodega_bodega__ = __webpack_require__(163);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the BodegaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BodegaPage = (function () {
    function BodegaPage(navCtrl, navParams, bodegaProvider, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.bodegaProvider = bodegaProvider;
        this.alertCtrl = alertCtrl;
        this.desplegar = false;
        this.listaBodegas = [];
        this.objCarga = {
            bodega: undefined,
            residuo: undefined,
            peso: 1
        };
    }
    BodegaPage.prototype.cambioTexto = function (texto) {
        var _this = this;
        this.desplegar = true;
        this.objCarga.bodega = undefined;
        this.bodegaProvider.buscarBodega(texto)
            .subscribe(function (response) {
            var r = JSON.parse(JSON.stringify(response));
            _this.listaBodegas = r.bodegas;
        }, function (error) {
        });
    };
    BodegaPage.prototype.itemSeleccionado = function (item) {
        this.objCarga.bodega = item;
        this.textoBusqueda = this.objCarga.bodega.name;
        this.desplegar = false;
    };
    BodegaPage.prototype.irAResiduos = function () {
        if (this.objCarga.bodega == undefined) {
            var alert_1 = this.alertCtrl.create({
                title: 'Atención',
                message: 'Debe seleccionar una bodega de la lista',
                buttons: ['Aceptar']
            });
            alert_1.present();
            return;
        }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__residuos_residuos__["a" /* ResiduosPage */], {
            paramObjCarga: this.objCarga
        });
    };
    BodegaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BodegaPage');
    };
    BodegaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-bodega',template:/*ion-inline-start:"/Users/ing_richardavid/Desktop/voltaJorge/src/pages/bodega/bodega.html"*/'<!--\n  Generated template for the BodegaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar color="primary">\n        <ion-title text-center>Bodega</ion-title>\n        \n        <ion-buttons end>\n            <button ion-button icon-only (click)="openModal()">\n                <ion-icon name="more"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n    <ion-grid>\n        <ion-row justify-content-center>\n            <ion-col col-10 text-center>\n                <ion-title text-center>Selecciona una Bodega</ion-title>\n            </ion-col>\n        </ion-row>\n        <ion-row justify-content-center>\n            <ion-col col-10 text-center>\n                <ion-item>\n                    <ion-label color="fgLabel" stacked text-left>Bodega</ion-label>\n                    <ion-input type="text" [(ngModel)]="textoBusqueda" (input)="cambioTexto($event.target.value)"></ion-input>\n                </ion-item>\n                <ion-list no-lines class="listaBusqueda" *ngIf="desplegar">\n                    <button ion-item *ngFor="let item of listaBodegas" (click)="itemSeleccionado(item)">{{ item.name }}</button>\n                </ion-list>\n            </ion-col>\n        </ion-row>\n        <ion-row justify-content-center>\n            <ion-col col-10 text-center>\n                <button ion-button color="button" block (click)="irAResiduos()">Siguiente</button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/ing_richardavid/Desktop/voltaJorge/src/pages/bodega/bodega.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_bodega_bodega__["a" /* BodegaProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], BodegaPage);
    return BodegaPage;
}());

//# sourceMappingURL=bodega.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResiduosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__peso_peso__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_residuos_residuos__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ResiduosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ResiduosPage = (function () {
    function ResiduosPage(navCtrl, loadingCtrl, navParams, residuosProvider, alertCtrl) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.residuosProvider = residuosProvider;
        this.alertCtrl = alertCtrl;
        this.listaResiduos = [];
        this.loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Por favor, espere...',
            dismissOnPageChange: true
        });
        this.objCarga = this.navParams.get('paramObjCarga');
    }
    ResiduosPage.prototype.irAPeso = function () {
        for (var i = 0; i < this.listaResiduos.length; i++) {
            if (this.listaResiduos[i].seleccionado != undefined && this.listaResiduos[i].seleccionado) {
                this.objCarga.residuo = this.listaResiduos[i];
                break;
            }
        }
        if (this.objCarga.residuo != undefined) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__peso_peso__["a" /* PesoPage */], {
                paramObjCarga: this.objCarga
            });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Atención',
                message: 'Seleccione al menos un tipo de residuo',
                buttons: ['Aceptar']
            });
            alert_1.present();
        }
    };
    ResiduosPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loading.present();
        this.residuosProvider.tiposResiduos()
            .subscribe(function (response) {
            var r = JSON.parse(JSON.stringify(response));
            _this.listaResiduos = r.tiposResiduo;
            _this.loading.dismiss();
        }, function (response) {
            _this.loading.dismiss();
            var alert = _this.alertCtrl.create({
                title: 'Error',
                message: 'No se puede conectar el servicio',
                buttons: ['Aceptar']
            });
            alert.present();
        });
    };
    ResiduosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-residuos',template:/*ion-inline-start:"/Users/ing_richardavid/Desktop/voltaJorge/src/pages/residuos/residuos.html"*/'<ion-header>\n    <ion-navbar color="primary">\n        <ion-title text-center>Residuos</ion-title>\n        \n        <ion-buttons end>\n            <button ion-button icon-only (click)="openModal()">\n                <ion-icon name="more"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n    <ion-grid>\n        <ion-row justify-content-center>\n            <ion-col col-10 text-center>\n                <ion-title text-center>Tipo de residuos</ion-title>\n            </ion-col>\n        </ion-row>\n        <ion-row justify-content-center>\n            <ion-col col-10 text-center>\n                <ion-list no-lines>\n                    <ion-item *ngFor="let item of listaResiduos">\n                        <ion-avatar item-start>{{ item.codigo }}</ion-avatar>\n                        <ion-label>{{ item.nombre }}</ion-label>\n                        <ion-checkbox [(ngModel)]="item.seleccionado" item-end></ion-checkbox>               \n                    </ion-item>\n                </ion-list>\n            </ion-col>\n        </ion-row>\n        <ion-row justify-content-center>\n            <ion-col col-10 text-center>\n                <button ion-button color="button" block (click)="irAPeso()">Siguiente</button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/ing_richardavid/Desktop/voltaJorge/src/pages/residuos/residuos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_residuos_residuos__["a" /* ResiduosProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ResiduosPage);
    return ResiduosPage;
}());

//# sourceMappingURL=residuos.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PesoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__resultado_resultado__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_residuos_residuos__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the PesoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PesoPage = (function () {
    function PesoPage(navCtrl, navParams, alertCtrl, residuosProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.residuosProvider = residuosProvider;
        this.objCarga = this.navParams.get('paramObjCarga');
    }
    PesoPage.prototype.irAResultado = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__resultado_resultado__["a" /* ResultadoPage */], {
            paramObjCarga: this.objCarga
        });
    };
    PesoPage.prototype.validarPeso = function () {
        var _this = this;
        if (this.objCarga.peso > 0) {
            this.residuosProvider.validarPeso(this.objCarga.peso)
                .subscribe(function (response) {
                console.log(response);
                _this.mostrarConfirmacion();
            }, function (error) {
                var alert = _this.alertCtrl.create({
                    title: 'Error',
                    message: 'No se puede conectar el servicio',
                    buttons: ['Aceptar']
                });
                alert.present();
            });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Atención',
                message: 'Debe colocar un peso mayor a 0 Kg',
                buttons: ['Aceptar']
            });
            alert_1.present();
        }
    };
    PesoPage.prototype.mostrarConfirmacion = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Atención',
            message: 'El tipo de residuos supera el máximo permitido',
            buttons: [{
                    text: 'Continuar',
                    handler: function () {
                        _this.irAResultado();
                    }
                }, {
                    text: 'Cancelar',
                    handler: function () {
                    }
                }]
        });
        confirm.present();
    };
    PesoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PesoPage');
    };
    PesoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-peso',template:/*ion-inline-start:"/Users/ing_richardavid/Desktop/voltaJorge/src/pages/peso/peso.html"*/'<!--\n  Generated template for the PesoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar color="primary">\n        <ion-title text-center>Peso</ion-title>\n        \n        <ion-buttons end>\n            <button ion-button icon-only (click)="openModal()">\n                <ion-icon name="more"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n    <ion-grid>\n        <ion-row justify-content-center>\n            <ion-col col-10 text-center>\n                <ion-title text-center>Cantidad en Kilos</ion-title>\n            </ion-col>\n        </ion-row>\n        <ion-row justify-content-center>\n            <ion-col col-10 text-center>\n                <ion-item>\n                    <ion-label color="fgLabel" stacked text-left>Peso en kg.</ion-label>\n                    <ion-input type="number" min="1" autocomplete="on" [(ngModel)]="objCarga.peso"></ion-input>\n                </ion-item>\n            </ion-col>\n        </ion-row>\n        <ion-row justify-content-center>\n            <ion-col col-10 text-center>\n                <button ion-button color="button" block (click)="validarPeso()" >Siguiente</button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/ing_richardavid/Desktop/voltaJorge/src/pages/peso/peso.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__providers_residuos_residuos__["a" /* ResiduosProvider */]])
    ], PesoPage);
    return PesoPage;
}());

//# sourceMappingURL=peso.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResultadoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_residuos_residuos__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ResultadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ResultadoPage = (function () {
    function ResultadoPage(navCtrl, alertCtrl, residuosProvider, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.residuosProvider = residuosProvider;
        this.navParams = navParams;
        this.objCarga = this.navParams.get('paramObjCarga');
    }
    ResultadoPage.prototype.irALogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    ResultadoPage.prototype.guardarCarga = function () {
        var _this = this;
        var oCarga = {
            codigoBodega: this.objCarga.bodega.code,
            codigoTipoResiduo: this.objCarga.residuo.codigo,
            peso: +this.objCarga.peso
        };
        this.residuosProvider.guardarCarga(oCarga)
            .subscribe(function (response) {
            var alert = _this.alertCtrl.create({
                title: 'Carga Exitosa',
                message: 'Los datos se han guardado correctamente',
                buttons: [{
                        text: 'Aceptar',
                        handler: function () {
                            _this.irALogin();
                        }
                    }]
            });
            alert.present();
        }, function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Error',
                message: 'No se puede conectar el servicio',
                buttons: ['Aceptar']
            });
            alert.present();
        });
    };
    ResultadoPage.prototype.mostrarConfirmacion = function () {
        var _this = this;
        var textoMensaje = "<ul>"
            + "<li><b>Bodega: </b>" + this.objCarga.bodega.name + "</li>"
            + "<li><b>Tipo: </b>" + this.objCarga.residuo.codigo + "</li>"
            + "<li><b>Kilo: </b>" + this.objCarga.peso + "</li>"
            + "</ul>";
        var confirm = this.alertCtrl.create({
            title: 'Datos de carga',
            message: textoMensaje,
            buttons: [{
                    text: 'Guardar',
                    handler: function () {
                        _this.guardarCarga();
                    }
                }, {
                    text: 'Cancelar',
                    handler: function () {
                        _this.navCtrl.pop();
                    }
                }]
        });
        confirm.present();
    };
    ResultadoPage.prototype.ionViewDidLoad = function () {
        this.mostrarConfirmacion();
    };
    ResultadoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-resultado',template:/*ion-inline-start:"/Users/ing_richardavid/Desktop/voltaJorge/src/pages/resultado/resultado.html"*/'<!--\n  Generated template for the ResultadoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar color="primary">\n        <ion-title text-center>Carga Lista</ion-title>\n        \n        <ion-buttons end>\n            <button ion-button icon-only (click)="openModal()">\n                <ion-icon name="more"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/ing_richardavid/Desktop/voltaJorge/src/pages/resultado/resultado.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_residuos_residuos__["a" /* ResiduosProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], ResultadoPage);
    return ResultadoPage;
}());

//# sourceMappingURL=resultado.js.map

/***/ }),

/***/ 120:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 120;

/***/ }),

/***/ 161:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/bodega/bodega.module": [
		288,
		4
	],
	"../pages/login/login.module": [
		289,
		0
	],
	"../pages/peso/peso.module": [
		290,
		3
	],
	"../pages/residuos/residuos.module": [
		291,
		2
	],
	"../pages/resultado/resultado.module": [
		292,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 161;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ws_ws__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var UserProvider = (function () {
    function UserProvider(ws) {
        this.ws = ws;
    }
    UserProvider.prototype.login = function (accountInfo) {
        return this.ws.post('login', accountInfo);
    };
    UserProvider.prototype.setUser = function (userData) {
        this.user = userData;
    };
    UserProvider.prototype.logout = function () {
        this.user = null;
    };
    UserProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ws_ws__["a" /* WsProvider */]])
    ], UserProvider);
    return UserProvider;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BodegaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ws_ws__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the BodegaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var BodegaProvider = (function () {
    function BodegaProvider(ws) {
        this.ws = ws;
        this.bodega = "";
    }
    BodegaProvider.prototype.buscarBodega = function (texto) {
        return this.ws.get('/bodega/' + texto);
    };
    BodegaProvider.prototype.setBodega = function (texto) {
        this.bodega = texto;
    };
    BodegaProvider.prototype.getBodega = function () {
        return this.bodega;
    };
    BodegaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ws_ws__["a" /* WsProvider */]])
    ], BodegaProvider);
    return BodegaProvider;
}());

//# sourceMappingURL=bodega.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(231);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bodega_bodega__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_residuos_residuos__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_peso_peso__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_resultado_resultado__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_ws_ws__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_login_login__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_user_user__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_bodega_bodega__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_residuos_residuos__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_bodega_bodega__["a" /* BodegaPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_residuos_residuos__["a" /* ResiduosPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_peso_peso__["a" /* PesoPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_resultado_resultado__["a" /* ResultadoPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/bodega/bodega.module#BodegaPageModule', name: 'BodegaPage', segment: 'bodega', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/peso/peso.module#PesoPageModule', name: 'PesoPage', segment: 'peso', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/residuos/residuos.module#ResiduosPageModule', name: 'ResiduosPage', segment: 'residuos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/resultado/resultado.module#ResultadoPageModule', name: 'ResultadoPage', segment: 'resultado', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_bodega_bodega__["a" /* BodegaPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_residuos_residuos__["a" /* ResiduosPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_peso_peso__["a" /* PesoPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_resultado_resultado__["a" /* ResultadoPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_13__providers_ws_ws__["a" /* WsProvider */],
                __WEBPACK_IMPORTED_MODULE_14__providers_login_login__["a" /* LoginProvider */],
                __WEBPACK_IMPORTED_MODULE_15__providers_user_user__["a" /* UserProvider */],
                __WEBPACK_IMPORTED_MODULE_16__providers_bodega_bodega__["a" /* BodegaProvider */],
                __WEBPACK_IMPORTED_MODULE_17__providers_residuos_residuos__["a" /* ResiduosProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/ing_richardavid/Desktop/voltaJorge/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/ing_richardavid/Desktop/voltaJorge/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/ing_richardavid/Desktop/voltaJorge/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Ionic Blank\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  The world is your oyster.\n  <p>\n    If you get lost, the <a href="http://ionicframework.com/docs/v2">docs</a> will be your guide.\n  </p>\n</ion-content>\n'/*ion-inline-end:"/Users/ing_richardavid/Desktop/voltaJorge/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LoginProvider = (function () {
    function LoginProvider(http) {
        this.http = http;
        console.log('Hello LoginProvider Provider');
    }
    LoginProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], LoginProvider);
    return LoginProvider;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WsProvider = (function () {
    function WsProvider(http) {
        this.http = http;
        this.url = '/api';
    }
    WsProvider.prototype.get = function (endpoint) {
        return this.http.get(this.url + '/' + endpoint);
    };
    WsProvider.prototype.post = function (endpoint, body) {
        return this.http.post(this.url + '/' + endpoint, body);
    };
    WsProvider.prototype.put = function (endpoint, body) {
        return this.http.put(this.url + '/' + endpoint, body);
    };
    WsProvider.prototype.delete = function (endpoint) {
        return this.http.delete(this.url + '/' + endpoint);
    };
    WsProvider.prototype.patch = function (endpoint, body) {
        return this.http.patch(this.url + '/' + endpoint, body);
    };
    WsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], WsProvider);
    return WsProvider;
}());

//# sourceMappingURL=ws.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResiduosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ws_ws__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the ResiduosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ResiduosProvider = (function () {
    function ResiduosProvider(ws) {
        this.ws = ws;
    }
    ResiduosProvider.prototype.guardarCarga = function (carga) {
        return this.ws.post('residuo', carga);
    };
    ResiduosProvider.prototype.tiposResiduos = function () {
        return this.ws.get('tipoResiduo');
    };
    ResiduosProvider.prototype.validarPeso = function (peso) {
        return this.ws.post('residuo/validarPeso/' + peso, {});
    };
    ResiduosProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ws_ws__["a" /* WsProvider */]])
    ], ResiduosProvider);
    return ResiduosProvider;
}());

//# sourceMappingURL=residuos.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__bodega_bodega__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_user__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = (function () {
    function LoginPage(navCtrl, loadingCtrl, userProvider, alertCtrl) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.userProvider = userProvider;
        this.alertCtrl = alertCtrl;
        this.account = {
            username: '',
            password: ''
        };
        this.loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Espere...',
            dismissOnPageChange: true
        });
    }
    LoginPage.prototype.ionViewDidLoad = function () {
    };
    LoginPage.prototype.irABodega = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__bodega_bodega__["a" /* BodegaPage */]);
    };
    LoginPage.prototype.ingresar = function () {
        var _this = this;
        if (this.account.username.length == 0 || this.account.password.length == 0) {
            var alert_1 = this.alertCtrl.create({
                title: 'Atención',
                message: 'Introduzca su nombre de usuario y contraseña',
                buttons: ['Aceptar']
            });
            alert_1.present();
            return;
        }
        this.loading.present().then(function () {
            _this.userProvider.login(_this.account)
                .subscribe(function (response) {
                var r = JSON.parse(JSON.stringify(response));
                if (r.user != undefined) {
                    _this.userProvider.setUser(r.user);
                    _this.irABodega();
                }
                else {
                    var alert_2 = _this.alertCtrl.create({
                        title: 'Atención',
                        message: 'El nombre de usuario y/o la contraseña son incorrectos',
                        buttons: ['Aceptar']
                    });
                    alert_2.present();
                }
            }, function (error) {
                console.log(error);
                var alert = _this.alertCtrl.create({
                    title: 'Error',
                    message: 'No se puede conectar con el servicio',
                    buttons: ['Aceptar']
                });
                alert.present();
            });
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/ing_richardavid/Desktop/voltaJorge/src/pages/login/login.html"*/'<ion-content>\n        <ion-grid>\n            <ion-row justify-content-center>\n                <ion-col col-8 text-center color="primary">\n                    <ion-img width="100%" color="primary" src="../assets/imgs/logo-white.png"></ion-img>\n                </ion-col>\n            </ion-row>\n            <ion-row justify-content-center>\n                <ion-col col-10 text-left>\n                    <div class="inputArea">\n                        <ion-icon name="person" text-center></ion-icon>\n                        <ion-input placeholder="Usuario" type="text" [(ngModel)]="account.username" name="username"></ion-input>\n                    </div>\n                </ion-col>\n            </ion-row>\n            <ion-row justify-content-center>\n                <ion-col col-10 text-left>\n                    <div class="inputArea">\n                        <ion-icon name="medical" text-center></ion-icon>\n                        <ion-input placeholder="Clave" type="password" [(ngModel)]="account.password" name="password"></ion-input>\n                    </div>\n                </ion-col>\n            </ion-row>\n            <ion-row justify-content-center>\n                <ion-col col-10 text-center>\n                    <button ion-button color="button" block (click)="ingresar()">INGRESAR</button>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/ing_richardavid/Desktop/voltaJorge/src/pages/login/login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__providers_user_user__["a" /* UserProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[211]);
//# sourceMappingURL=main.js.map