import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { BodegaPage } from '../pages/bodega/bodega';
import { ResiduosPage } from '../pages/residuos/residuos';
import { PesoPage } from '../pages/peso/peso';
import { ResultadoPage } from '../pages/resultado/resultado';
import { WsProvider } from '../providers/ws/ws';
import { LoginProvider } from '../providers/login/login';
import { UserProvider } from '../providers/user/user';
import { BodegaProvider } from '../providers/bodega/bodega';
import { ResiduosProvider } from '../providers/residuos/residuos';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    BodegaPage,
    ResiduosPage,
    PesoPage,
    ResultadoPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    BodegaPage,
    ResiduosPage,
    PesoPage,
    ResultadoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    WsProvider,
    LoginProvider,
    UserProvider,
    BodegaProvider,
    ResiduosProvider
  ]
})
export class AppModule {}
