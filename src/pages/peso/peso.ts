import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ResultadoPage } from '../resultado/resultado';

import { ResiduosProvider } from '../../providers/residuos/residuos';
/**
 * Generated class for the PesoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-peso',
  templateUrl: 'peso.html',
})
export class PesoPage {

    objCarga: { bodega: any, residuo: any, peso: number};
    
    constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public residuosProvider: ResiduosProvider) {
        this.objCarga = this.navParams.get('paramObjCarga');
    }

    irAResultado() {
        this.navCtrl.push(ResultadoPage, {
           paramObjCarga : this.objCarga
        });
    }
    
    validarPeso() {
        if(this.objCarga.peso > 0){
            this.residuosProvider.validarPeso(this.objCarga.peso)
            .subscribe((response) => {
                console.log(response);
                this.mostrarConfirmacion();
            }, (error) => {
                let alert = this.alertCtrl.create({
                    title: 'Error',
                    message: 'No se puede conectar el servicio',
                    buttons: ['Aceptar']
                });
                alert.present();
            });
        } else {
            let alert = this.alertCtrl.create({
                title: 'Atención',
                message: 'Debe colocar un peso mayor a 0 Kg',
                buttons: ['Aceptar']
            });
            alert.present();
        }
    }
  
    mostrarConfirmacion() {
        let confirm = this.alertCtrl.create({
            title: 'Atención',
            message: 'El tipo de residuos supera el máximo permitido',
            buttons: [{
                text: 'Continuar',
                handler: () => {
                    this.irAResultado();
                }
            },{
                text: 'Cancelar',
                handler: () => {

                }
            }]
        });
        confirm.present();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PesoPage');
  }

}
