import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, AlertController } from 'ionic-angular';

import { MainPage } from '../pages';
import { BodegaPage } from '../bodega/bodega';

import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  account: { username: string, password: string } = {
    username: '',
    password: ''
  };
  
  loading: any;

    constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public userProvider: UserProvider, public alertCtrl: AlertController) {
        this.loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Espere...',
            dismissOnPageChange: true
        });
    }
  
  ionViewDidLoad() {
  }
  
  irABodega() {
    this.navCtrl.push(BodegaPage);
  }

  ingresar() {
    if(this.account.username.length == 0 || this.account.password.length == 0){
        let alert = this.alertCtrl.create({
            title: 'Atención',
            message: 'Introduzca su nombre de usuario y contraseña',
            buttons: ['Aceptar']
        });
        alert.present();
        return;
    }
    this.loading.present().then(() => {
        this.userProvider.login(this.account)
        .subscribe((response) => {
            let r = JSON.parse(JSON.stringify(response));
            if(r.user != undefined){
                this.userProvider.setUser(r.user);
                this.irABodega();
            } else {
                let alert = this.alertCtrl.create({
                    title: 'Atención',
                    message: 'El nombre de usuario y/o la contraseña son incorrectos',
                    buttons: ['Aceptar']
                });
                alert.present();
            }
        }, (error) => {
            console.log(error);
            let alert = this.alertCtrl.create({
                title: 'Error',
                message: 'No se puede conectar con el servicio',
                buttons: ['Aceptar']
            });
            alert.present();
        });
    })
  }
}
