import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { PesoPage } from '../peso/peso';

import { ResiduosProvider } from '../../providers/residuos/residuos';

/** 
 * Generated class for the ResiduosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-residuos',
  templateUrl: 'residuos.html',
})
export class ResiduosPage {

    listaResiduos: any = [];
    loading: any;      
      
    objCarga: { bodega: any, residuo: any, peso: number};

    constructor(public navCtrl: NavController,
                public loadingCtrl: LoadingController,
                public navParams: NavParams,
                public residuosProvider: ResiduosProvider,
                public alertCtrl: AlertController) {
        this.loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Por favor, espere...',
            dismissOnPageChange: true
        });
        this.objCarga = this.navParams.get('paramObjCarga');
    }

    irAPeso() {
        for(let i=0; i < this.listaResiduos.length; i++){
            if(this.listaResiduos[i].seleccionado != undefined && this.listaResiduos[i].seleccionado){
                this.objCarga.residuo = this.listaResiduos[i];
                break;
            }
        }
        if(this.objCarga.residuo != undefined){
            this.navCtrl.push(PesoPage, {
                paramObjCarga : this.objCarga
            });
        } else {
            let alert = this.alertCtrl.create({
                title: 'Atención',
                message: 'Seleccione al menos un tipo de residuo',
                buttons: ['Aceptar']
            });
            alert.present();
        }
    }

    ionViewDidLoad() {
        this.loading.present();
        this.residuosProvider.tiposResiduos()
        .subscribe((response) => {
            let r = JSON.parse(JSON.stringify(response));
            this.listaResiduos = r.tiposResiduo;
            this.loading.dismiss();
        }, (response) => {
            this.loading.dismiss();
            let alert = this.alertCtrl.create({
                title: 'Error',
                message: 'No se puede conectar el servicio',
                buttons: ['Aceptar']
            });
            alert.present();
        });
    }

}
