import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ResiduosPage } from '../residuos/residuos';

import { BodegaProvider } from '../../providers/bodega/bodega';
/**
 * Generated class for the BodegaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bodega',
  templateUrl: 'bodega.html',
})
export class BodegaPage {

    textoBusqueda: string;

    desplegar: boolean = false; 
    
    listaBodegas: any[] = [];

    objCarga: { bodega: any, residuo: any, peso: number} = {
        bodega: undefined,
        residuo: undefined,
        peso: 1
    };

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public bodegaProvider: BodegaProvider,
                public alertCtrl: AlertController) {
    }

    cambioTexto(texto: string) {
        this.desplegar = true;
        this.objCarga.bodega = undefined;
        this.bodegaProvider.buscarBodega(texto)
        .subscribe((response) => {
            let r = JSON.parse(JSON.stringify(response));
            this.listaBodegas = r.bodegas;
        }, (error) => {
        });
    }
    
    itemSeleccionado(item: any){
        this.objCarga.bodega = item;
        this.textoBusqueda = this.objCarga.bodega.name;
        this.desplegar = false;
    }

    irAResiduos() {
        if(this.objCarga.bodega == undefined){
            let alert = this.alertCtrl.create({
                title: 'Atención',
                message: 'Debe seleccionar una bodega de la lista',
                buttons: ['Aceptar']
            });
            alert.present();
            return;
        }
        this.navCtrl.push(ResiduosPage, {
           paramObjCarga : this.objCarga
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad BodegaPage');
    }

}
