import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LoginPage } from '../login/login';

import { ResiduosProvider } from '../../providers/residuos/residuos';
/**
 * Generated class for the ResultadoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resultado',
  templateUrl: 'resultado.html',
})
export class ResultadoPage {

    objCarga: { bodega: any, residuo: any, peso: number};
    
    constructor(public navCtrl: NavController,
                public alertCtrl: AlertController,
                public residuosProvider: ResiduosProvider,
                public navParams: NavParams) {
        this.objCarga = this.navParams.get('paramObjCarga');
    }

    irALogin() {
        this.navCtrl.push(LoginPage);
    }
    
    guardarCarga() {
        let oCarga = {
            codigoBodega: this.objCarga.bodega.code,
            codigoTipoResiduo: this.objCarga.residuo.codigo,
            peso: +this.objCarga.peso
        };
        this.residuosProvider.guardarCarga(oCarga)
        .subscribe((response) => {
            let alert = this.alertCtrl.create({
                title: 'Carga Exitosa',
                message: 'Los datos se han guardado correctamente',
                buttons: [{
                    text: 'Aceptar',
                    handler: () => {
                        this.irALogin()
                    }
                }]
            });
            alert.present();
        }, (error) => {
            let alert = this.alertCtrl.create({
                title: 'Error',
                message: 'No se puede conectar el servicio',
                buttons: ['Aceptar']
            });
            alert.present();
        });
    }
    
    mostrarConfirmacion() {
        let textoMensaje = "<ul>"
            +"<li><b>Bodega: </b>"+ this.objCarga.bodega.name + "</li>"
            +"<li><b>Tipo: </b>" + this.objCarga.residuo.codigo +"</li>"
            +"<li><b>Kilo: </b>" + this.objCarga.peso + "</li>"
            +"</ul>";
        let confirm = this.alertCtrl.create({
            title: 'Datos de carga',
            message: textoMensaje,
            buttons: [{
                text: 'Guardar',
                handler: () => {
                    this.guardarCarga();
                }
            },{
                text: 'Cancelar',
                handler: () => {
                   this.navCtrl.pop();
                }
            }]
        });
        confirm.present();
    }

    ionViewDidLoad() {
        this.mostrarConfirmacion();
    }

}
