import { Injectable } from '@angular/core';
import { WsProvider } from '../ws/ws';
/*
  Generated class for the BodegaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BodegaProvider {

    bodega: string = "";

    constructor(public ws: WsProvider) {
    }

    buscarBodega(texto: any) {
        return this.ws.get('/bodega/' + texto);
    }
    
    setBodega(texto: string) {
        this.bodega = texto;
    }
    
    getBodega() {
        return this.bodega;
    }

}
