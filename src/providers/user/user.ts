import { Injectable } from '@angular/core';
import { WsProvider } from '../ws/ws';
/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

    user: any;

    constructor(public ws: WsProvider) {
    }

    login(accountInfo: any) {
        return this.ws.post('login', accountInfo);
    }
  
    setUser(userData: any) {
        this.user = userData;
    }

    logout() {
        this.user = null;
    }

}
