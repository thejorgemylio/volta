import { HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

@Injectable()
export class WsProvider {

  url: string = '/api';

  constructor(public http: HttpClient) {
  }

    get(endpoint: string) {
        return this.http.get(this.url + '/' + endpoint);
    }

    post(endpoint: string, body: any) {
        return this.http.post(this.url + '/' + endpoint, body);
    }

    put(endpoint: string, body: any) {
        return this.http.put(this.url + '/' + endpoint, body);
    }

    delete(endpoint: string) {
        return this.http.delete(this.url + '/' + endpoint);
    }

    patch(endpoint: string, body: any) {
        return this.http.patch(this.url + '/' + endpoint, body);
    }

}
