import { Injectable } from '@angular/core';
import { WsProvider } from '../ws/ws';
/*
  Generated class for the ResiduosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ResiduosProvider {

    constructor(public ws: WsProvider) {
    }

    guardarCarga(carga) {
        return this.ws.post('residuo', carga);
    }

    tiposResiduos() {
        return this.ws.get('tipoResiduo');
    }
      
    validarPeso(peso) {
        return this.ws.post('residuo/validarPeso/' + peso, {});
    }
  
}
